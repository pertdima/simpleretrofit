package binar.co.id.simplekotlinretrofit.section.home

import binar.co.id.simplekotlinretrofit.model.StudentResponse
import binar.co.id.simplekotlinretrofit.network.NetworkServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by pertadima on 14,December,2018
 */

class MainPresenter(private val service: NetworkServices, private val mainView: MainView) {

    fun getAllStudent() {
        service.fetchAllStudent().enqueue(object : Callback<StudentResponse> {
            override fun onResponse(call: Call<StudentResponse>, response: Response<StudentResponse>) {
                if (response.code() == 200) {
                    response.body()?.data?.let {
                        mainView.onSuccessGetStudent(it.toMutableList())
                    }
                }
            }

            override fun onFailure(call: Call<StudentResponse>, t: Throwable) {

            }
        })
    }
}