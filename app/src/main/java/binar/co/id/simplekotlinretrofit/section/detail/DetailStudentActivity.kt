package binar.co.id.simplekotlinretrofit.section.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import binar.co.id.simplekotlinretrofit.R
import binar.co.id.simplekotlinretrofit.model.DetailStudentResponse
import binar.co.id.simplekotlinretrofit.model.StudentModel
import binar.co.id.simplekotlinretrofit.section.MainApp
import kotlinx.android.synthetic.main.activity_detail_student.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailStudentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_student)
        requestServices(intent.getIntExtra("id", 0))
    }

    private fun requestServices(id: Int) {
        val call = MainApp().services.searchStudent(id)
        call.enqueue(object : Callback<DetailStudentResponse> {
            override fun onResponse(call: Call<DetailStudentResponse>, response: Response<DetailStudentResponse>) {
                if (response.code() == 200) {
                    response.body()?.data?.let {
                        getData(it)
                    }
                }
            }

            override fun onFailure(call: Call<DetailStudentResponse>, t: Throwable) {

            }
        })
    }

    private fun getData(data: StudentModel?) {
        progress_bar.visibility = View.GONE
        tv_name.text = data?.name
        tv_email.text = data?.email
    }
}
