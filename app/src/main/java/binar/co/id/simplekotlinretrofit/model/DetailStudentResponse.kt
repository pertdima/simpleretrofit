package binar.co.id.simplekotlinretrofit.model

import com.google.gson.annotations.SerializedName

/**
 * Created by pertadima on 22,October,2018
 */

data class DetailStudentResponse(@SerializedName("status") val status: String?,
                                 @SerializedName("data") val data: StudentModel?,
                                 @SerializedName("error") val error: String)