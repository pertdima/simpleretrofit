package binar.co.id.simplekotlinretrofit.section.home

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import binar.co.id.simplekotlinretrofit.R
import binar.co.id.simplekotlinretrofit.model.StudentModel
import binar.co.id.simplekotlinretrofit.section.detail.DetailStudentActivity
import kotlinx.android.synthetic.main.viewholder_student.view.*

/**
 * Created by pertadima on 12,October,2018
 */

class MainAdapter(val data: List<StudentModel>, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.viewholder_student, p0, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> holder.bindView(data[position])
        }
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val textMain = view.tv_name?.let {
            it
        }

        private val textEmail = view.tv_email?.let {
            it
        }

        fun bindView(data: StudentModel) {
            textMain?.text = data.name
            textEmail?.text = data.email
            itemView.setOnClickListener {
                context.startActivity(Intent(context, DetailStudentActivity::class.java).apply {
                    putExtra("id", data.id)
                })
            }
        }
    }
}