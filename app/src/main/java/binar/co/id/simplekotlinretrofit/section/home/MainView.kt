package binar.co.id.simplekotlinretrofit.section.home

import binar.co.id.simplekotlinretrofit.model.StudentModel

/**
 * Created by pertadima on 14,December,2018
 */
interface MainView {
    fun onSuccessGetStudent(data: MutableList<StudentModel>)
}