package binar.co.id.simplekotlinretrofit.model

import com.google.gson.annotations.SerializedName

/**
 * Created by pertadima on 22,October,2018
 */

class StudentRequest(@SerializedName("email") val email: String,
                     @SerializedName("name") val name: String)