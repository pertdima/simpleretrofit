package binar.co.id.simplekotlinretrofit.section

import android.app.Application
import binar.co.id.simplekotlinretrofit.BuildConfig
import binar.co.id.simplekotlinretrofit.network.NetworkServices
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by pertadima on 22,October,2018
 */

class MainApp : Application() {
    val client = OkHttpClient().newBuilder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            })
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

    val retrofit = Retrofit.Builder()
            .baseUrl("https://kotlinspringcrud.herokuapp.com/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    val services = retrofit.create(NetworkServices::class.java)
}