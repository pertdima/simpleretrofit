package binar.co.id.simplekotlinretrofit.network

import binar.co.id.simplekotlinretrofit.model.DetailStudentResponse
import binar.co.id.simplekotlinretrofit.model.StudentRequest
import binar.co.id.simplekotlinretrofit.model.StudentResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

/**
 * Created by pertadima on 22,October,2018
 */
interface NetworkServices {
    @GET("api/v1/student/all")
    fun fetchAllStudent(): Call<StudentResponse>

    @GET("/api/v1/student/{id}")
    fun searchStudent(@Path("id") id: Int): Call<DetailStudentResponse>

    @POST("/api/v1/student")
    fun postStudent(@Body studentRequest: StudentRequest): Call<DetailStudentResponse>

    @PUT("/api/v1/student/{id}")
    fun putStudent(@Path("id") id: Int, @Body studentRequest: StudentRequest)
            : Call<DetailStudentResponse>

    @DELETE("/api/v1/student/{id}")
    fun deleteStudent(@Path("id") id: Int): Call<DetailStudentResponse>
}