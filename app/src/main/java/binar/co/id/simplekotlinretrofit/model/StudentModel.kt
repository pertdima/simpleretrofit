package binar.co.id.simplekotlinretrofit.model

import com.google.gson.annotations.SerializedName

/**
 * Created by pertadima on 22,October,2018
 */

data class StudentModel(@SerializedName("id") val id: Int?,
                        @SerializedName("name") val name: String?,
                        @SerializedName("email") val email: String?)