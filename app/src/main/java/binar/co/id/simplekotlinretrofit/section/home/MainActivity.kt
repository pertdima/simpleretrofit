package binar.co.id.simplekotlinretrofit.section.home

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import binar.co.id.simplekotlinretrofit.R
import binar.co.id.simplekotlinretrofit.model.StudentModel
import binar.co.id.simplekotlinretrofit.model.StudentResponse
import binar.co.id.simplekotlinretrofit.section.MainApp
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), MainView {
    private val listStudent = mutableListOf<StudentModel>()
    private val adapterMain = MainAdapter(listStudent, this@MainActivity)

    lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecyclerView()
        mainPresenter = MainPresenter(MainApp().services, this)
        mainPresenter.getAllStudent()
    }

    private fun initRecyclerView() {
        with(rv_main) {
            adapter = adapterMain
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    override fun onSuccessGetStudent(data: MutableList<StudentModel>) {
        progress_bar.visibility = View.GONE
        listStudent.clear()
        listStudent.addAll(data)
        adapterMain.notifyDataSetChanged()
    }
}
